### * new_screen()

#' Create a new projection screen
#'
#' A projection screen is an object of class \code{screen} and is an in-memory
#' raster canvas to which shapes can be drawn using the \code{shine_*}
#' functions. The current content of a \code{screen} can be accessed with the
#' \code{screen2rgb} function and can be displayed with the \code{display}
#' function.
#'
#' Note that the inner implementation of a \code{screen} object is subject to
#' change. End-users should not access or modify directly the content of a
#' \code{screen} object, but should use the getter and setter functions
#' instead.
#' 
#' @param width Screen width in pixels.
#' @param height Screen height in pixels.
#' @param xlim Limits of the x axis (default \code{c(0, 1)}).
#' @param ylim Limits of the y axis (default \code{c(0, 1)}).
#'
#' @return A new \code{screen} object.
#'
#' @export

new_screen <- function(width, height, xlim = c(0, 1), ylim = c(0, 1)) {
    if (missing(width) | missing(height)) {
        stop("Both `width` and `height` must be provided.")
    }
    # Basic attributes
    x <- list("width_px" = width,
              "height_px" = height,
              "xlim" = xlim,
              "ylim" = ylim)
    # The screen "canvas" element stores the luminance values for the three
    # channels: red, green, and blue. It stores it as a vector which can be
    # used to fill a raster object using row-major order (see
    # https://ggfx.data-imaginist.com/articles/custom_filters.html for more
    # explanations about the nativeRaster format).
    black_canvas <- rep(0, width * height)
    color_canvas <- list("red" = black_canvas,
                         "green" = black_canvas,
                         "blue" = black_canvas)
    x[["canvas"]] <- color_canvas
    # Return
    x <- structure(x, class = c("screen", class(x)))
    return(x)
}

### * light_pixels()

#' Apply light onto individual pixels
#'
#' @param screen \code{screen} object.
#' @param x,y X and y indices of the pixels to apply light onto. Note that the
#'     top-left corner of the screen is at (1,1) and the bottom-right corner at
#'     (width_px,height_px) of the screen.
#' @param col Color to apply to the pixel. It can be a single value or a vector
#'     of colors of the same length as x and y.
#' @param clip Boolean, if \code{TRUE} (the default) x and y indices that are
#'     outside the screen limits will be disregarded. If \code{FALSE}, an error
#'     will be thrown if any x or y value is outside the screen limits.
#'
#' @return An updated \code{screen} object.
#'
#' @examples
#' z <- new_screen(width = 10, height = 10)
#' x <- sample(1:10, 20, replace = TRUE)
#' y <- sample(1:10, 20, replace = TRUE)
#' col <- sample(c("chartreuse", "orchid1", "deepskyblue"), 20,
#'               replace = TRUE)
#' z <- light_pixels(z, x, y, col)
#' display(z)
#'
#' @export

light_pixels <- function(screen, x, y, col, clip = TRUE) {
    stopifnot(length(x) == length(y))
    if (length(col) == 1) col <- rep(col, length(x))
    stopifnot(length(col) == length(x))
    if (!clip) {
        stopifnot(all(x >= 1))
        stopifnot(all(x <= screen[["width_px"]]))
        stopifnot(all(y >= 1))
        stopifnot(all(y <= screen[["height_px"]]))
    }
    kept_indices <- which(x >= 1 & x <= screen[["width_px"]] &
                          y >= 1 & y <= screen[["height_px"]])
    if (length(kept_indices) == 0) return(screen)
    x <- x[kept_indices]
    y <- y[kept_indices]
    col <- col[kept_indices]
    col_rgb <- farver::decode_colour(col)
    # Note that the canvas is actually built as a transposed matrix of the
    # screen pixels, since this will allow to respect the screen canvas
    # row-major order when adding the luminance values.
    canvas <- matrix(0,
                     nrow = screen[["width_px"]],
                     ncol = screen[["height_px"]])
    canvas_red <- canvas
    canvas_green <- canvas
    canvas_blue <- canvas
    # Set pixels
    for (i in seq_along(x)) {
        canvas_red[x[i], y[i]] <- canvas_red[x[i], y[i]] + col_rgb[i, 1]
        canvas_green[x[i], y[i]] <- canvas_green[x[i], y[i]] + col_rgb[i, 2]
        canvas_blue[x[i], y[i]] <- canvas_blue[x[i], y[i]] + col_rgb[i, 3]
    }
    # Add luminances
    screen[["canvas"]][["red"]] <- (screen[["canvas"]][["red"]] +
                                    as.vector(canvas_red))
    screen[["canvas"]][["green"]] <- (screen[["canvas"]][["green"]] +
                                      as.vector(canvas_green))
    screen[["canvas"]][["blue"]] <- (screen[["canvas"]][["blue"]] +
                                     as.vector(canvas_blue))
    # Return
    screen
}

### * shine_lines()

#' Draw lines onto a projection screen
#'
#' Note that with this function, a set of lines drawn with one function call
#' does not "self-overlap". In other words, a line does not get brighter where
#' it self-intersects. Use the function \code{shine_lines_step_by_step()} for
#' this kind of self-overlapping effect, and also to allow for variable line
#' width and line color.
#'
#' @param screen A \code{screen} object.
#' @param x X coordinates.
#' @param y Y coordinates.
#' @param col Line color (one value).
#' @param lwd Line width (one value).
#'
#' @return An updated \code{screen} object.
#'
#' @examples
#' x <- new_screen(150, 100)
#' x <- shine_lines(x, runif(20), runif(20), col = "yellow", lwd = 5)
#' x <- shine_lines(x, seq(0, 1, length.out = 128),
#'        0.55 + 1/5 * sin(seq(0, 1, length.out = 128) * 6 * pi),
#'        col = "purple", lwd = 3)
#' display(x)
#'
#' @export

shine_lines <- function(screen, x, y, col = "white", lwd = 1) {
    canvas <- prep_ragg_canvas_from_screen(screen)
    graphics::lines(x = x, y = y, col = col, lwd = lwd)
    apply_ragg_canvas_on_screen(canvas, screen)
}

### * shine_lines_step_by_step()

#' Draw lines onto a projection screen
#'
#' @param screen A \code{screen} object.
#' @param x X coordinates.
#' @param y Y coordinates.
#' @param col Line color (one value or a vector of values). 
#' @param lwd Line width (one value or a vector of values).
#'
#' @return An updated \code{screen} object.
#'
#' @examples
#' z <- new_screen(150, 100)
#' z <- shine_lines_step_by_step(z, x = runif(20), y = runif(20),
#'        col = "orchid4", lwd = 5)
#' z <- shine_lines_step_by_step(z, x = seq(0, 1, length.out = 128),
#'        y = 0.55 + 1/5 * sin(seq(0, 1, length.out = 128) * 6 * pi),
#'        col = rainbow(127), lwd = 1 + abs(4 * sin(1:127/(1.2*pi))))
#' display(z)
#'
#' @export

shine_lines_step_by_step <- function(screen, x, y, col = "white",
                                     lwd = 1) {
    canvas <- prep_ragg_canvas_from_screen(screen)
    xlim <- screen[["xlim"]]
    ylim <- screen[["ylim"]]
    stopifnot(length(x) > 1)
    if (length(col) == 1) col <- rep(col, length(x) - 1)
    if (length(lwd) == 1) lwd <- rep(lwd, length(x) - 1)
    stopifnot(length(col) == length(x)-1)
    stopifnot(length(lwd) == length(x)-1)
    for (i in 2:length(x)) {
        graphics::rect(xlim[1], ylim[1], xlim[2], ylim[2], col = "black", border = "black")
        graphics::lines(x = x[(i-1):i], y = y[(i-1):i], col = col[i-1],
                        lwd = lwd[i-1])
        screen <- apply_ragg_canvas_on_screen(canvas, screen, dev.off = FALSE)
    }
    grDevices::dev.off()
    screen
}

### * shine_rect

### * shine_polygon()

#' Draw a polygon onto a projection screen
#'
#' @param screen A \code{screen} object.
#' @param x X coordinates.
#' @param y Y coordinates.
#' @param border Color of the border.
#' @param col Fill color.
#' @param lwd Line width.
#'
#' @return An updated \code{screen} object.
#'
#' @examples
#' x <- new_screen(100, 100)
#' x <- shine_polygon(x, runif(20), runif(20), border = "green", col = "yellow",
#'        lwd = 3)
#' display(x)
#'
#' @export

shine_polygon <- function(screen, x, y, border = "white", col = "black",
                          lwd = 1) {
    canvas <- prep_ragg_canvas_from_screen(screen)
    graphics::polygon(x = x, y = y, border = border, col = col, lwd = lwd)
    apply_ragg_canvas_on_screen(canvas, screen)
}

### * shine_circle()

#' Draw a circle onto a projection screen
#'
#' @param screen A \code{screen} object.
#' @param x X coordinates of center.
#' @param y Y coordinates of center.
#' @param radius Radius.
#' @param border Color of the border.
#' @param col Fill color.
#' @param lwd Line width.
#' @param steps Integer, number of steps to draw the circle.
#'
#' @return An updated \code{screen} object.
#'
#' @examples
#' x <- new_screen(100, 100)
#' x <- shine_circle(x, 0.5, 0.5, 0.3, border = "green", col = "yellow",
#'        lwd = 3, steps = 9)
#' display(x)
#' x <- shine_circle(x, 0.25, 0.15, 0.6, border = "blue", col = "purple",
#'        lwd = 3)
#' display(x)
#'
#' @export

shine_circle <- function(screen, x, y, radius, border = "white", col = "black",
                         lwd = 1, steps = 36) {
    angles <- seq(0, 2*pi, length.out = steps + 1)[1:steps]
    xs <- x + cos(angles) * radius
    ys <- y + sin(angles) * radius
    shine_polygon(screen = screen, x = xs, y = ys, border = border, col = col,
                  lwd = lwd)
}

### * shine_text()

#' Write text on a screen
#'
#' For more details about the parameters other than \code{screen}, see the
#' corresponding parameters in \code{grid::grid.text}.
#'
#' @param screen A \code{screen} object.
#' @param labels Text to write.
#' @param x,y Coordinates.
#' @param adj One or two values in (0, 1) to specify x and (optionally) y
#'     adjustments. See the \code{adj} parameter in \code{?text}.
#' @param pos A position parameter for the text. See the \code{pos} parameter
#'     in \code{?text}.
#' @param srt Rotation angle.
#' @param cex Size. Note that the effect of this parameter depends on the size
#'     of the screen (width_px and height_px).
#' @param col Color.
#' @param ... Other parameters are passed to \code{text()}.
#'
#' @return An updated \code{screen} object.
#'
#' @examples
#' z <- new_screen(160, 100)
#' z <- shine_text(z, "Lantern", 0.5, 0.5, col = "red", cex = 1.5)
#' z <- shine_text(z, "Lantern", 0.55, 0.45, col = "green", cex = 2, srt = 25, family = "serif")
#' z <- shine_text(z, "Lantern", 0.2, 0.7, col = "cyan", cex = 2, family = "monospace")
#' display(z, interpolate = FALSE)
#'
#' @export

shine_text <- function(screen, labels, x, y, adj = NULL, pos = NULL, srt = 0,
                       cex = 1, col = "white", ...) {
    canvas <- prep_ragg_canvas_from_screen(screen)
    graphics::text(labels = labels, x = x, y = y, adj = adj, pos = pos,
                   srt = srt, cex = cex, col = col, ...)
    apply_ragg_canvas_on_screen(canvas, screen)
}

### * shine_gaussian()

#' Shine one or several Gaussian spots onto a screen
#'
#' @param screen A \code{screen} object.
#' @param x,y X and Y coordinates of the Gaussian center(s).
#' @param sd Standard deviation(s) of the Gaussian(s).
#' @param intensity Light intensity at the Gaussian peak(s).
#' @param col Color(s) of the Gaussian(s).
#'
#' @return An updated \code{screen} object.
#'
#' @examples
#' z <- new_screen(200, 100, xlim = c(0, 2), ylim = c(0, 1))
#' z <- shine_gaussian(z, 0.4, 0.2, sd = 0.1, col = "orange")
#' display(z)
#'
#' grid::grid.newpage()
#' set.seed(6)
#' z <- new_screen(100, 100)
#' n <- 20
#' z <- shine_gaussian(z, runif(n), runif(n),
#'        sd = runif(n, 0.05, 0.2), intensity = runif(n),
#'        col = sample(c("red", "green"), n, replace = TRUE))
#' display(z, interpolate = FALSE)
#'
#' @export

shine_gaussian <- function(screen, x, y, sd = 1, intensity = 1,
                           col = "white") {
    stopifnot(length(x) == length(y))
    if (length(sd) == 1) sd <- rep(sd, length(x))
    if (length(intensity) == 1) intensity <- rep(intensity, length(x))
    if (length(col) == 1) col <- rep(col, length(x))
    stopifnot(length(sd) == length(x))
    stopifnot(length(intensity) == length(x))
    stopifnot(length(col) == length(x))
    intensities <- lapply(seq_along(x), function(i) {
        o <- matrix(0, nrow = screen[["width_px"]], ncol = screen[["height_px"]])
        x_rows <- (diff(screen[["xlim"]]) / screen[["width_px"]] *
                   (1:screen[["width_px"]] - 0.5) + screen[["xlim"]][1])
        y_cols <- rev(diff(screen[["ylim"]]) / screen[["height_px"]] *
                      (1:screen[["height_px"]] - 0.5) + screen[["ylim"]][1])
        x_sq_dist <- (x_rows - x[i])^2
        y_sq_dist <- (y_cols - y[i])^2
        for (k in seq_len(ncol(o))) {
            for (l in seq_len(nrow(o))) {
                o[l,k] <- sqrt(x_sq_dist[l] + y_sq_dist[k])
            }
        }
        o <- stats::dnorm(o, mean = 0, sd = sd[i]) * intensity[i]
    })
    frames <- lapply(seq_along(x), function(i) {
        o <- matrix(col[i], ncol = screen[["width_px"]], nrow = screen[["height_px"]])
        o <- farver::decode_colour(o)/255 * as.vector(intensities[[i]])
        colnames(o) <- c("red", "green", "blue")
        o
    })
    for (i in seq_along(frames)) {
        for (channel in c("red", "green", "blue")) {
            screen[["canvas"]][[channel]] <- (screen[["canvas"]][[channel]] +
                                              frames[[i]][, channel])
        }
    }
    screen
}

### * screen2rgb

#' Convert a \code{screen} object to a matrix of colors
#'
#' @param screen A \code{screen} object.
#' @param max Maximum value, passed as \code{maxColorValue} to
#'     \code{grDevices::rgb}. If \code{NULL} (the default), then the max value
#'     is determined from the screen \code{canvas} values.
#'
#' @return A matrix of the same dimensions (width, hehght) as the
#'     \code{screen}, containing hexadecimal color values. This matrix is ready
#'     to be used with, for example, \code{grid::grid.raster}.
#'
#' @examples
#' z <- primary_colors(8, 8, display = FALSE)
#' w <- screen2rgb(z)
#' w
#' grid::grid.raster(w, interpolate = FALSE)
#'
#' @export

screen2rgb <- function(screen, max = NULL) {
    if (is.null(max)) {
        max <- max(unlist(screen[["canvas"]]))
    }
    inv_g <- 1 / default_gamma()
    colors <- grDevices::rgb(
         red = t(matrix(screen[["canvas"]][["red"]]^(inv_g), ncol = screen[["height_px"]])),
         green = t(matrix(screen[["canvas"]][["green"]]^(inv_g), ncol = screen[["height_px"]])),
         blue = t(matrix(screen[["canvas"]][["blue"]]^(inv_g), ncol = screen[["height_px"]])),
         maxColorValue = max^(inv_g)
      )
    matrix(colors, nrow = screen[["height_px"]])
}

### * display

#' Display a \code{screen} object (using \code{grid::grid.raster})
#'
#' @param screen A \code{screen} object.
#' @param interpolate Boolean (default: \code{FALSE}).
#' @param max Passed to \code{screen2rgb}.
#' @param ... Passed to \code{grid::grid.raster}.
#'
#' @return Called for its side effect of calling \code{grid::grid.raster} (no
#'     return value).
#'
#' @export

display <- function(screen, interpolate = FALSE, max = NULL, ...) {
    z <- screen2rgb(screen, max = max)
    grid::grid.raster(z, interpolate = FALSE, ...)
}
