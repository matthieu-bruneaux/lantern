[![GitLab pipeline status](https://gitlab.com/matthieu-bruneaux/lantern/badges/master/pipeline.svg)](https://gitlab.com/matthieu-bruneaux/lantern/commits/master)
[![Coverage report](https://gitlab.com/matthieu-bruneaux/lantern/badges/master/coverage.svg)](https://matthieu-bruneaux.gitlab.io/lantern/coverage/coverage.html)
[![R_CMD_CHECK](https://matthieu-bruneaux.gitlab.io/lantern/R-CMD-check_badge.svg)](https://matthieu-bruneaux.gitlab.io/lantern/R-CMD-check_output.txt)
[![Lifecycle Status](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://www.tidyverse.org/lifecycle/)

lantern: an R package to draw images with additive color mixing <img src="man/figures/hexsticker_lantern.png" width="120" align="right" />
===================================================

`lantern` provides a simple toolkit to perform additive color mixing when drawing an image.

## Installation

You can install the `lantern` package from R with:

```
install.packages("devtools")
devtools::install_gitlab("matthieu-bruneaux/lantern", quiet = TRUE)
```

The package is not on CRAN at the moment, but hopefully it will be submitted once a stable first version is achieved.

## Documentation

Have a look at the [vignettes](https://matthieu-bruneaux.gitlab.io/lantern/articles/index.html) to learn how to use the package!
