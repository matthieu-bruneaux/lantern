### * Description

# Test the behaviour of package dependencies.

### * farver

test_that("farver returns expected values", {
    # Decode several colors
    expected <- structure(c(255L, 255L, 0L, 127L, 100L, 0L, 192L, 0L, 255L,
                            149L, 0L, 203L, 255L, 0L, 237L), .Dim = c(5L, 3L),
                          .Dimnames = list(NULL, c("r", "g", "b")))
    observed <- farver::decode_colour(c("red", "pink", "blue", "chartreuse",
                                        "cornflowerblue"))
    expect_identical(observed, expected)
    # Decode a single color
    expected <- structure(c(100L, 149L, 237L), .Dim = c(1L, 3L),
                          .Dimnames = list(NULL, c("r", "g", "b")))
    observed <- farver::decode_colour("cornflowerblue")
    expect_identical(observed, expected)
})
